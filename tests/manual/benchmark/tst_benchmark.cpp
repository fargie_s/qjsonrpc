/*
 * Copyright (C) 2012-2013 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qjsonrpc
 *
 * This file is part of the QJsonRpc Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
#include <QScopedPointer>

#include <QtCore/QEventLoop>
#include <QtCore/QVariant>
#include <QtTest/QtTest>
#include <QElapsedTimer>
#include <QThread>

#if QT_VERSION >= 0x050000
#include <QJsonDocument>
#else
#include "json/qjsondocument.h"
#endif

#include "qjsonrpcabstractserver.h"
#include "qjsonrpcsocket.h"
#include "qjsonrpcmessage.h"

class TestBenchmark: public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void simple();
    void namedParameters();

};

class TestService : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("serviceName", "service")
public:
    TestService(QObject *parent = 0) : QObject(parent)
    {}

public Q_SLOTS:
    QString singleParam(int i) const { return QString::number(i); }
    QString singleParam(const QString &string) const { return string; }
    QString singleParam(const QVariant &k) const { return k.toString(); }

    QString namedParams(int integer, const QString &string, double doub)
    {
        (void) integer;
        (void) doub;

        return string;
    }
};

void TestBenchmark::simple()
{
    QJsonRpcServiceProvider provider;
    TestService service;
    provider.registerObject(&service);

    QJsonRpcMessage request =
        QJsonRpcMessage::createRequest("service.singleParam", QString("test"));
    QBENCHMARK {
        QVERIFY(provider.processMessage(request).isValid());
    }
}

void TestBenchmark::namedParameters()
{
    QJsonRpcServiceProvider provider;
    TestService service;
    provider.registerObject(&service);

    QJsonObject obj;
    obj["integer"] = 1;
    obj["string"] = QLatin1String("str");
    obj["doub"] = 1.2;
    QJsonRpcMessage request =
        QJsonRpcMessage::createRequest("service.namedParams", obj);

    QBENCHMARK {
        QVERIFY(provider.processMessage(request).isValid());
    }
}

QTEST_MAIN(TestBenchmark)
#include "tst_benchmark.moc"

