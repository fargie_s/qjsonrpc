/*
 * Copyright (C) 2012-2013 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qjsonrpc
 *
 * This file is part of the QJsonRpc Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
#include <QtCore/QVariant>
#include <QtTest/QtTest>

#if QT_VERSION >= 0x050000
#include <QJsonDocument>
#else
#include "json/qjsondocument.h"
#endif

#include "qjsonrpcabstractserver.h"
#include "qjsonrpcmessage.h"

class TestQJsonRpcService: public QObject
{
    Q_OBJECT  
private slots:
    void dispatch();
    void ambiguousDispatch();
    void dispatchSignals();

};

class TestService : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("serviceName", "service")
public:
    TestService(QObject *parent = 0)
        : QObject(parent),
          m_stringCount(0),
          m_intCount(0),
          m_variantCount(0)
    {}

    int stringCount() const { return m_stringCount; }
    int intCount() const { return m_intCount; }
    int variantCount() const { return m_variantCount; }
    void resetCounters() { m_stringCount = m_intCount = m_variantCount = 0; }

Q_SIGNALS:
    void testSignal();
    void testSignalWithParameter(const QString &param);

public Q_SLOTS:
    QString testMethod(const QString &string) const {
        return string;
    }

    // note: order of definition matters here for ambiguousDispatch test
    void ambiguousMethod(const QString &) {
        m_stringCount++;
    }

    void ambiguousMethod(int) {
        m_intCount++;
    }

    void ambiguousMethod(const QVariant &) {
        m_variantCount++;
    }

private:
    int m_stringCount;
    int m_intCount;
    int m_variantCount;

};

class TestServiceProvider : public QJsonRpcServiceProvider
{
    Q_OBJECT
public:
    TestServiceProvider() {}

public Q_SLOTS:
    void notify(const QJsonRpcMessage &notification)
    {
        this->notification = notification;
    }

public:
    QJsonRpcMessage notification;
};

void TestQJsonRpcService::dispatch()
{
    TestServiceProvider provider;
    TestService service;
    QJsonRpcMessage reply;
    provider.registerObject(&service);

    QJsonRpcMessage validRequestDispatch =
        QJsonRpcMessage::createRequest("service.testMethod", QLatin1String("testParam"));
    reply = provider.processMessage(validRequestDispatch);
    QVERIFY(reply.isValid());
    QCOMPARE(reply.result().toString(), QLatin1String("testParam"));

    QJsonObject namedParameters;
    namedParameters.insert("string", QLatin1String("testParam"));
    QJsonRpcMessage validRequestDispatchWithNamedParameters =
        QJsonRpcMessage::createRequest("service.testMethod", namedParameters);
    reply = provider.processMessage(validRequestDispatchWithNamedParameters);
    QVERIFY(reply.isValid());
    QCOMPARE(reply.result().toString(), QLatin1String("testParam"));

    QJsonObject invalidNamedParameters;
    invalidNamedParameters.insert("testParameter", QLatin1String("testParam"));
    QJsonRpcMessage invalidRequestDispatchWithNamedParameters =
        QJsonRpcMessage::createRequest("service.testMethod", invalidNamedParameters);
    reply = provider.processMessage(invalidRequestDispatchWithNamedParameters);
    QCOMPARE(reply.type(), QJsonRpcMessage::Error);
    QCOMPARE(static_cast<QJsonRpc::ErrorCode>(reply.errorCode()), QJsonRpc::InvalidParams);

    QJsonRpcMessage validNotificationDispatch =
        QJsonRpcMessage::createNotification("service.testMethod", QLatin1String("testParam"));
    reply = provider.processMessage(validNotificationDispatch);
    QVERIFY(!reply.isValid()); /* No reply for notifications */

    QJsonRpcMessage validNotificationDispatchWithNamedParameters =
        QJsonRpcMessage::createNotification("service.testMethod", namedParameters);
    reply = provider.processMessage(validNotificationDispatchWithNamedParameters);
    QVERIFY(!reply.isValid()); /* No reply for notifications */

    QJsonRpcMessage invalidResponseDispatch =
        validRequestDispatch.createResponse(QLatin1String("testResult"));
    reply = provider.processMessage(invalidResponseDispatch);
    QCOMPARE(reply.type(), QJsonRpcMessage::Error);
    QCOMPARE(static_cast<QJsonRpc::ErrorCode>(reply.errorCode()), QJsonRpc::InvalidRequest);

    QJsonRpcMessage invalidDispatch;
    reply = provider.processMessage(invalidDispatch);
    QCOMPARE(reply.type(), QJsonRpcMessage::Error);
    QCOMPARE(static_cast<QJsonRpc::ErrorCode>(reply.errorCode()), QJsonRpc::InvalidRequest);
}

void TestQJsonRpcService::ambiguousDispatch()
{
    TestServiceProvider provider;
    TestService service;
    provider.registerObject(&service);

    QJsonRpcMessage stringDispatch =
        QJsonRpcMessage::createRequest("service.ambiguousMethod", QLatin1String("testParam"));
    provider.processMessage(stringDispatch);
    QCOMPARE(service.stringCount(), 1);
    QCOMPARE(service.intCount(), 0);
    QCOMPARE(service.variantCount(), 0);

    QJsonRpcMessage intDispatch =
        QJsonRpcMessage::createRequest("service.ambiguousMethod", 10);
    provider.processMessage(intDispatch);
    QCOMPARE(service.stringCount(), 1);
    QCOMPARE(service.intCount(), 1);
    QCOMPARE(service.variantCount(), 0);

    QStringList stringList = QStringList() << "test" << "string" << "list";
    QJsonRpcMessage stringListDispatch =
        QJsonRpcMessage::createRequest("service.ambiguousMethod", QJsonValue::fromVariant(stringList));
    provider.processMessage(stringListDispatch);
    QCOMPARE(service.stringCount(), 1);
    QCOMPARE(service.intCount(), 1);
    QCOMPARE(service.variantCount(), 1);
}

void TestQJsonRpcService::dispatchSignals()
{
    TestServiceProvider provider;
    TestService service;
    QJsonRpcMessage reply;
    provider.registerObject(&service);

    service.testSignal();
    QVERIFY(provider.notification.isValid());
    QCOMPARE(provider.notification.method(), QString("testSignal"));
    QVERIFY(provider.notification.params().isUndefined());

    service.testSignalWithParameter("testsigparam");
    QVERIFY(provider.notification.isValid());
    QCOMPARE(provider.notification.method(), QString("testSignalWithParameter"));
    QVERIFY(provider.notification.params().isObject());
    QJsonObject obj = provider.notification.params().toObject();
    QCOMPARE(obj.value("param").toString(), QString("testsigparam"));
}

QTEST_MAIN(TestQJsonRpcService)
#include "tst_qjsonrpcservice.moc"
