include(../qjsonrpc.pri)

INCLUDEPATH += .
TEMPLATE = lib
TARGET = qjsonrpc
QT += core core-private network
QT -= gui
DEFINES += QJSONRPC_BUILD
CONFIG += $${QJSONRPC_LIBRARY_TYPE}
VERSION = $${QJSONRPC_VERSION}
win32:DESTDIR = $$OUT_PWD

# check if we need to build qjson
lessThan(QT_MAJOR_VERSION, 5) {
    include(json/json.pri)
}

PRIVATE_HEADERS += \
    qjsonrpcserviceprovider_p.h \
    qjsonrpcsocket_p.h \
    qjsonrpcabstractserver_p.h \
    qjsonrpcservicereply_p.h

INSTALL_HEADERS += \
    qjsonrpcmessage.h \
    qjsonrpcserviceprovider.h \
    qjsonrpcsocket.h \
    qjsonrpcabstractserver.h \
    qjsonrpclocalserver.h \
    qjsonrpctcpserver.h \
    qjsonrpc_export.h \
    qjsonrpcservicereply.h \
    qjsonrpchttpclient.h

greaterThan(QT_MAJOR_VERSION, 4) {
    greaterThan(QT_MINOR_VERSION, 1) {
        INSTALL_HEADERS += qjsonrpcmetatype.h
    }
}

HEADERS += \
    $${INSTALL_HEADERS} \
    $${PRIVATE_HEADERS}

SOURCES += \
    qjsonrpcmessage.cpp \
    qjsonrpcserviceprovider.cpp \
    qjsonrpcsocket.cpp \
    qjsonrpcabstractserver.cpp \
    qjsonrpclocalserver.cpp \
    qjsonrpctcpserver.cpp \
    qjsonrpcservicereply.cpp \
    qjsonrpchttpclient.cpp

EXTRAMOC_SOURCES = qjsonrpcserviceprovider.cpp

load(moc)
# extra moc for custom object metatypes
extramoc.CONFIG = no_link
extramoc.dependency_type = TYPE_C
extramoc.output = $$MOC_DIR/$${QMAKE_CPP_MOD_MOC}${QMAKE_FILE_BASE}.extramoc
extramoc.commands = $$moc_source.commands && $$QMAKE_STREAM_EDITOR -i \'s/::qt_static_metacall(/::qt_static_metacall_real(/\' ${QMAKE_FILE_OUT}
extramoc.input = EXTRAMOC_SOURCES
extramoc.name = EXTRAMOC ${QMAKE_FILE_IN}
extramoc.variable_out = EXTRAMOC
QMAKE_EXTRA_COMPILERS += extramoc

# install
headers.files = $${INSTALL_HEADERS}
headers.path = $${PREFIX}/include/qjsonrpc
qjson_headers.files = $${QJSON_INSTALL_HEADERS}
qjson_headers.path = $${PREFIX}/include/qjsonrpc/json
private_headers.files = $${PRIVATE_HEADERS}
private_headers.path = $${PREFIX}/include/qjsonrpc/private
target.path = $${PREFIX}/$${LIBDIR}
INSTALLS += headers qjson_headers private_headers target

# pkg-config support
CONFIG += create_pc create_prl no_install_prl
QMAKE_PKGCONFIG_DESTDIR = pkgconfig
QMAKE_PKGCONFIG_LIBDIR = $$target.path
QMAKE_PKGCONFIG_INCDIR = $$headers.path
equals(QJSONRPC_LIBRARY_TYPE, staticlib) {
    QMAKE_PKGCONFIG_CFLAGS = -DQJSONRPC_STATIC
} else {
    QMAKE_PKGCONFIG_CFLAGS = -DQJSONRPC_SHARED
}
unix:QMAKE_CLEAN += -r pkgconfig lib$${TARGET}.prl

