/*
 * Copyright (C) 2012-2013 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qjsonrpc
 *
 * This file is part of the QJsonRpc Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
#ifndef QJSONRPCSERVICE_H
#define QJSONRPCSERVICE_H

#include <QObject>

#include "qjsonrpcmessage.h"

class QJsonRpcServiceProviderPrivate;

class QJSONRPC_EXPORT QJsonRpcServiceProvider : public QObject
{
    Q_OBJECT
public:
    explicit QJsonRpcServiceProvider(QObject *parent = 0);
    virtual ~QJsonRpcServiceProvider();

    virtual bool registerObject(QObject *object);
    virtual bool unregisterObject(QObject *object);

public Q_SLOTS:
    /**
     * @brief process an incoming message
     * @param[in] request the message to process.
     * @return
     *  - QJsonRpcMessage::Error message on error.
     *  - QJsonRpcMessage::Response message on success.
     *  - QJsonRpcMessage::Invalid when no reply is requested
     *      or if the reply will be sent later on.
     */
    virtual QJsonRpcMessage processMessage(const QJsonRpcMessage &request);

    /**
     * @brief send a notification.
     * @param[in] notification the notification to send.
     */
    virtual void notify(const QJsonRpcMessage &notification);

protected:
    explicit QJsonRpcServiceProvider(QJsonRpcServiceProviderPrivate &dd, QObject *parent);

    Q_DECLARE_PRIVATE(QJsonRpcServiceProvider)
    Q_DISABLE_COPY(QJsonRpcServiceProvider)
    Q_PRIVATE_SLOT(d_func(), void _q_objectDestroyed(QObject *object))
};

#endif

