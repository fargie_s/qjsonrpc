/*
 * Copyright (C) 2012-2014 Matt Broadstone
 * Copyright (C) 2013 Fargier Sylvain
 * Contact: http://bitbucket.org/devonit/qjsonrpc
 *
 * This file is part of the QJsonRpc Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QObject>
#include <QVarLengthArray>
#include <QMetaMethod>
#include <QEventLoop>
#include <QDebug>
#include <QThread>

#include "qjsonrpcsocket.h"
#include "qjsonrpcserviceprovider.h"
#include "qjsonrpcserviceprovider_p.h"

static int cachedRelaySlotMethodIndex = -1;

class QJsonRpcRelay : public QObject
{
public:
    Q_OBJECT
    Q_DECL_HIDDEN_STATIC_METACALL static void qt_static_metacall_real(QObject *, QMetaObject::Call, int, void **);

public:
    QJsonRpcRelay(QObject *parent = 0) :
        QObject(parent)
    {}
    virtual ~QJsonRpcRelay()
    {}

    void relaySlot(void **a);

    static int relaySlotMethodIndex()
    {
        if (cachedRelaySlotMethodIndex == -1) {
            cachedRelaySlotMethodIndex = staticMetaObject.indexOfMethod("relaySlot()");
            Q_ASSERT(cachedRelaySlotMethodIndex != -1);
        }
        return cachedRelaySlotMethodIndex;
    }

public Q_SLOTS:
    void relaySlot() {}

Q_SIGNALS:
    void notify(const QJsonRpcMessage &message);
};

static inline QByteArray methodName(const QJsonRpcMessage &request)
{
    const QString &methodPath(request.method());
    return methodPath.midRef(methodPath.lastIndexOf('.') + 1).toLatin1();
}

static inline QByteArray serviceName(const QJsonRpcMessage &message)
{
    return message.method().section(".", 0, -2).toLatin1();
}

QJsonRpcServiceProviderPrivate::QJsonRpcServiceProviderPrivate() :
    relay(new QJsonRpcRelay)
{
}

QJsonRpcServiceProviderPrivate::~QJsonRpcServiceProviderPrivate()
{
    foreach (QJsonRpcSlotCache *cacheItem, cache)
        delete cacheItem;
    delete relay;
}

QByteArray QJsonRpcServiceProviderPrivate::serviceName(const QObject *service) const
{
    const QMetaObject *mo = service->metaObject();
    for (int i = 0; i < mo->classInfoCount(); i++) {
        const QMetaClassInfo mci = mo->classInfo(i);
        if (mci.name() == QLatin1String("serviceName"))
            return mci.value();
    }

    return QByteArray(mo->className()).toLower();
}

QJsonRpcSlotCache *QJsonRpcServiceProviderPrivate::slotCache(const QObject *obj)
{
    const QMetaObject *meta = obj->metaObject();
    QJsonRpcSlotCache *&cacheItem = cache[meta];
    if (!cacheItem)
    {
        cacheItem = new QJsonRpcSlotCache;
        int startIdx = QObject::staticMetaObject.methodCount(); // skip QObject slots
        for (int idx = startIdx; idx < meta->methodCount(); ++idx) {
            const QMetaMethod method = meta->method(idx);
            if (method.methodType() == QMetaMethod::Slot &&
                 method.access() == QMetaMethod::Public) {

#if QT_VERSION >= 0x050000
                QByteArray signature = method.methodSignature();
                QByteArray methodName = method.name();
#else
                QByteArray signature = method.signature();
                QByteArray methodName = signature.left(signature.indexOf('('));
#endif

                QJsonRpcSlotCache::MethodInfo info(method);
                if (!info.valid)
                    continue;

                if (signature.contains("QVariant"))
                    cacheItem->hash[methodName].append(info);
                else
                    cacheItem->hash[methodName].prepend(info);
            }
        }
    }
    return cacheItem;
}


QJsonRpcSlotCache::ParameterInfo::ParameterInfo(const QString &n, int t, bool o)
    : type(t),
      jsType(QJsonRpcServiceProviderPrivate::convertVariantTypeToJSType(t)),
      name(n),
      out(o)
{
}

QJsonRpcSlotCache::MethodInfo::MethodInfo()
    : returnType(QMetaType::Void),
      valid(false),
      hasOut(false)
{
}

QJsonRpcSlotCache::MethodInfo::MethodInfo(const QMetaMethod &method)
    : returnType(QMetaType::Void),
      valid(true),
      hasOut(false),
      index(method.methodIndex())
{
#if QT_VERSION >= 0x050000
    returnType = method.returnType();
    if (returnType == QMetaType::UnknownType) {
        qWarning() << "QJsonRpcService: can't bind method's return type"
                      << QString(method.name());
        valid = false;
        return;
    }

    parameters.reserve(method.parameterCount());
#else
    returnType = QMetaType::type(method.typeName());
    parameters.reserve(method.parameterNames().count());
#endif

    const QList<QByteArray> &types = method.parameterTypes();
    const QList<QByteArray> &names = method.parameterNames();
    for (int i = 0; i < types.size(); ++i) {
        QByteArray parameterType = types.at(i);
        const QByteArray &parameterName = names.at(i);
        bool out = parameterType.endsWith('&');

        if (out) {
            hasOut = true;
            parameterType.resize(parameterType.size() - 1);
        }
        int type = QMetaType::type(parameterType);
        if (type == 0) {
            qWarning() << "QJsonRpcService: can't bind method's parameter"
                          << QString(parameterType);
            valid = false;
            break;
        }

        parameters.append(ParameterInfo(parameterName, type, out));
    }
}

QJsonRpcServiceProvider::QJsonRpcServiceProvider(QObject *parent)
    : QObject(*new QJsonRpcServiceProviderPrivate, parent)
{
    Q_D(QJsonRpcServiceProvider);
    connect(d->relay, SIGNAL(notify(QJsonRpcMessage)),
            this, SLOT(notify(QJsonRpcMessage)));
}


QJsonRpcServiceProvider::QJsonRpcServiceProvider(QJsonRpcServiceProviderPrivate &dd, QObject *parent)
    : QObject(dd, parent)
{
}

QJsonRpcServiceProvider::~QJsonRpcServiceProvider()
{
}


int QJsonRpcServiceProviderPrivate::convertVariantTypeToJSType(int type)
{
    switch (type) {
    case QMetaType::Int:
    case QMetaType::UInt:
    case QMetaType::Double:
    case QMetaType::Long:
    case QMetaType::LongLong:
    case QMetaType::Short:
    case QMetaType::Char:
    case QMetaType::ULong:
    case QMetaType::ULongLong:
    case QMetaType::UShort:
    case QMetaType::UChar:
    case QMetaType::Float:
        return QJsonValue::Double;    // all numeric types in js are doubles
    case QMetaType::QVariantList:
    case QMetaType::QStringList:
        return QJsonValue::Array;
    case QMetaType::QVariantMap:
        return QJsonValue::Object;
    case QMetaType::QString:
        return QJsonValue::String;
    case QMetaType::Bool:
        return QJsonValue::Bool;
    default:
        break;
    }

    return QJsonValue::Undefined;
}

int QJsonRpcServiceProviderPrivate::qjsonRpcMessageType = qRegisterMetaType<QJsonRpcMessage>("QJsonRpcMessage");

static bool jsParameterCompare(const QJsonArray &parameters,
                               const QJsonRpcSlotCache::MethodInfo &info)
{
    int j = 0;
    for (int i = 0; i < info.parameters.size() && j < parameters.size(); ++i) {
        int jsType = info.parameters.at(i).jsType;
        if (jsType != QJsonValue::Undefined && jsType != parameters.at(j).type()) {
            if (!info.parameters.at(i).out)
                return false;
        }
        else {
            ++j;
        }
    }

    return (j == parameters.size());
}

static  bool jsParameterCompare(const QJsonObject &parameters,
                                const QJsonRpcSlotCache::MethodInfo &info)
{
    for (int i = 0; i < info.parameters.size(); ++i) {
        int jsType = info.parameters.at(i).jsType;
        QJsonValue value = parameters.value(info.parameters.at(i).name);
        if (value == QJsonValue::Undefined) {
            if (!info.parameters.at(i).out)
                return false;
        } else if (jsType == QJsonValue::Undefined) {
            continue;
        } else if (jsType != value.type()) {
            return false;
        }
    }

    return true;
}

static inline QVariant convertArgument(const QJsonValue &argument,
                                       const QJsonRpcSlotCache::ParameterInfo &info)
{
    if (argument.isUndefined())
        return QVariant(info.type, NULL);

#if QT_VERSION >= 0x050200
    if (info.type == QMetaType::QJsonValue || info.type == QMetaType::QVariant ||
        info.type >= QMetaType::User) {
        QVariant result(argument);
        if (info.type >= QMetaType::User && result.canConvert(info.type))
            result.convert(info.type);
        return result;
    }

    QVariant result = argument.toVariant();
    if (result.userType() == info.type || info.type == QMetaType::QVariant) {
        return result;
    } else if (result.canConvert(info.type)) {
        result.convert(info.type);
        return result;
    } else if (info.type < QMetaType::User) {
        // already tried for >= user, this is the last resort
        QVariant result(argument);
        if (result.canConvert(info.type)) {
            result.convert(info.type);
            return result;
        }
    }

    return QVariant();
#else
    QVariant result = argument.toVariant();
    QVariant::Type variantType = static_cast<QVariant::Type>(info.type);
    if (info.type != QMetaType::QVariant && info.type != result.type() &&
        !result.canConvert(variantType))
        return QVariant();

    if (!result.canConvert(variantType)) {
        // toVariant succeeded, no need to convert
        return result;
    }

    result.convert(variantType);
    return result;
#endif
}

static inline QJsonValue convertReturnValue(QVariant &returnValue)
{
#if QT_VERSION >= 0x050200
    switch (returnValue.type()) {
    case QMetaType::Bool:
    case QMetaType::Int:
    case QMetaType::Double:
    case QMetaType::LongLong:
    case QMetaType::ULongLong:
    case QMetaType::UInt:
    case QMetaType::QString:
    case QMetaType::QStringList:
    case QMetaType::QVariantList:
    case QMetaType::QVariantMap:
        return QJsonValue::fromVariant(returnValue);
    default:
        // if a conversion operator was registered it will be used
        if (returnValue.convert(QMetaType::QJsonValue))
            return returnValue.toJsonValue();
        else
            return QJsonValue();
    }
#else
    // custom conversions could not be registered before 5.2, so this is only an optimization
    return QJsonValue::fromVariant(returnValue);
#endif
}

static inline QJsonValue convertValue(void *val, int type)
{
    switch (type) {
    case QMetaType::Bool:
        return QJsonValue(*reinterpret_cast<bool *>(val));
    case QMetaType::Int:
        return QJsonValue((double) *reinterpret_cast<int *>(val));
    case QMetaType::Double:
        return QJsonValue(*reinterpret_cast<double *>(val));
    case QMetaType::LongLong:
        return QJsonValue((double) *reinterpret_cast<long long *>(val));
    case QMetaType::ULongLong:
        return QJsonValue((double) *reinterpret_cast<unsigned long long *>(val));
    case QMetaType::UInt:
        return QJsonValue((double) *reinterpret_cast<unsigned int *>(val));
    case QMetaType::QString:
        return QJsonValue(*reinterpret_cast<QString *>(val));
    case QMetaType::QStringList:
        return QJsonValue(QJsonArray::fromStringList(*reinterpret_cast<QStringList *>(val)));
    case QMetaType::QVariantList:
        return QJsonValue(QJsonArray::fromVariantList(*reinterpret_cast<QVariantList *>(val)));
    case QMetaType::QVariantMap:
        return QJsonValue(QJsonObject::fromVariantMap(*reinterpret_cast<QVariantMap *>(val)));
    default:
        {
            QJsonValue value;
            return QMetaType::convert(val, type, &value, QMetaType::QJsonValue);
        }
    }
}

QJsonRpcMessage QJsonRpcServiceProvider::processMessage(const QJsonRpcMessage &message)
{
    Q_D(QJsonRpcServiceProvider);
    switch (message.type()) {
        case QJsonRpcMessage::Request:
        case QJsonRpcMessage::Notification: {
            QByteArray srvName = serviceName(message);
            QObject *object = 0;
            if (!srvName.isEmpty()) /* is empty service allowed ? */
                object = d->services.value(srvName);

            if (!object) {
                if (message.type() == QJsonRpcMessage::Request) {
                    return message.createErrorResponse(
                        QJsonRpc::MethodNotFound,
                        QString("service '%1' not found").arg(srvName.constData()));
                }
                else
                    return QJsonRpcMessage(); /* FIXME: handle notifications asynchronously */
            } else {
                /* FIXME find sub-object */
                return d->deliverCall(object, message);
            }
        }
        break;

        case QJsonRpcMessage::Response:
            // we don't handle responses in the provider
        default:
            return message.createErrorResponse(QJsonRpc::InvalidRequest,
                                               QString("invalid request"));
            break;
    };
}

void QJsonRpcServiceProvider::notify(const QJsonRpcMessage &)
{
}

bool QJsonRpcServiceProvider::registerObject(QObject *service)
{
    Q_D(QJsonRpcServiceProvider);
    QByteArray serviceName = d->serviceName(service);
    if (serviceName.isEmpty()) {
        qDebug() << Q_FUNC_INFO << "service added without serviceName classinfo, aborting";
        return false;
    }

    if (d->services.contains(serviceName)) {
        qDebug() << Q_FUNC_INFO << "service with name " << serviceName << " already exist";
        return false;
    }

    d->slotCache(service);
    d->services.insert(serviceName, service);
    connect(service, SIGNAL(destroyed(QObject*)),
            this, SLOT(_q_objectDestroyed(QObject *)),
            Qt::DirectConnection);
    QMetaObject::connect(service, -1,
                         d->relay, d->relay->relaySlotMethodIndex(),
                         Qt::DirectConnection);
    return true;
}

bool QJsonRpcServiceProvider::unregisterObject(QObject *service)
{
    Q_D(QJsonRpcServiceProvider);
    QByteArray serviceName = d->serviceName(service);
    QHash<QByteArray, QObject *>::iterator it = d->services.find(serviceName);
    if (it == d->services.end()) {
        qDebug() << Q_FUNC_INFO << "can not find service with name"
                 << serviceName;
        return false;
    }
    else if (it.value() != service) {
        qDebug() << Q_FUNC_INFO << "can not find service with name"
                 << serviceName << "(another instance is active)";
        return false;
    }
    else {
        d->services.erase(it);
        service->disconnect(this, SLOT(_q_objectDestroyed(QObject *)));
        QMetaObject::disconnect(service, -1, d->relay, d->relay->relaySlotMethodIndex());
        return true;
    }
}

QJsonRpcMessage QJsonRpcServiceProviderPrivate::deliverCall(QObject *object, const QJsonRpcMessage &request)
{
    if (request.type() != QJsonRpcMessage::Request &&
        request.type() != QJsonRpcMessage::Notification) {
        return request.createErrorResponse(QJsonRpc::InvalidRequest,
                                           "invalid request");
    }

    const QByteArray &method(methodName(request));
    QJsonRpcSlotCache *cache = slotCache(object);

    QJsonRpcSlotCache::Hash::const_iterator it = cache->hash.constFind(method);
    if (it == cache->hash.constEnd())
    {
        return request.createErrorResponse(QJsonRpc::MethodNotFound,
                                           "invalid method called");
    }

    QVariantList arguments;
    const QJsonValue &params = request.params();
    bool usingNamedParameters = params.isObject();
    QVarLengthArray<void *, 10> parameters;
    QVariant returnValue;
    QMetaType::Type returnType = QMetaType::Void;

    foreach (const QJsonRpcSlotCache::MethodInfo &info, it.value())
    {
        bool methodMatch = usingNamedParameters ?
            jsParameterCompare(params.toObject(), info) :
            jsParameterCompare(params.toArray(), info);

        if (!methodMatch)
            continue;

        arguments.reserve(info.parameters.size());
        returnType = static_cast<QMetaType::Type>(info.returnType);
        returnValue = (returnType == QMetaType::Void) ?
                       QVariant() : QVariant(returnType, NULL);
        if (returnType == QMetaType::QVariant)
            parameters.append(&returnValue);
        else
            parameters.append(returnValue.data());

        for (int i = 0; i < info.parameters.size(); ++i) {
            const QJsonRpcSlotCache::ParameterInfo &parameterInfo = info.parameters.at(i);
            QJsonValue incomingArgument = usingNamedParameters ?
                params.toObject().value(parameterInfo.name) :
                params.toArray().at(i);

            QVariant argument = convertArgument(incomingArgument, parameterInfo);
            if (!argument.isValid()) {
                QString message = incomingArgument.isUndefined() ?
                            QString("failed to construct default object for '%1'").arg(parameterInfo.name) :
                            QString("failed to convert from JSON for '%1'").arg(parameterInfo.name);
                return request.createErrorResponse(QJsonRpc::InvalidParams, message);
            }

            arguments.push_back(argument);
            if (parameterInfo.type == QMetaType::QVariant)
                parameters.append(static_cast<void *>(&arguments.last()));
            else
                parameters.append(const_cast<void *>(arguments.last().constData()));
        }
        if (object->qt_metacall(QMetaObject::InvokeMetaMethod,
                                info.index, parameters.data()) >= 0) {
            QString message = QString("dispatch for method '%1' failed").arg(method.constData());
            return request.createErrorResponse(QJsonRpc::InvalidRequest, message);
        }
        else if (info.hasOut) {
            QJsonArray ret;
            if (info.returnType != QMetaType::Void)
                ret.append(convertReturnValue(returnValue));
            for (int i = 0; i < info.parameters.size(); ++i)
                if (info.parameters.at(i).out)
                    ret.append(convertReturnValue(arguments[i]));
            if (ret.size() > 1)
                return request.createResponse(ret);
            else
                return request.createResponse(ret.first());
        }
        else {
            return request.createResponse(convertReturnValue(returnValue));
        }
    }

    return request.createErrorResponse(QJsonRpc::InvalidParams,
                                       "invalid parameters");
}

void QJsonRpcServiceProviderPrivate::_q_objectDestroyed(QObject *object)
{
    Q_Q(QJsonRpcServiceProvider);
    q->unregisterObject(object);
}

void QJsonRpcRelay::qt_static_metacall(QObject *o, QMetaObject::Call c, int id, void **a)
{
    if ((c == QMetaObject::InvokeMetaMethod) && ((id + staticMetaObject.methodOffset()) == relaySlotMethodIndex()))
    {
        QJsonRpcRelay *_t = static_cast<QJsonRpcRelay *>(o);
        _t->relaySlot(a);
    }
    QJsonRpcRelay::qt_static_metacall_real(o, c, id, a);
}

void QJsonRpcRelay::relaySlot(void **a)
{
    QObject *sndr = sender();
    if (Q_LIKELY(sndr))
    {
        int idx = senderSignalIndex();
        if (idx < QObject::staticMetaObject.methodCount())
            // QObject signal (destroyed(QObject *)) -- ignore
            return;

        const QMetaObject *senderMetaObject = sndr->metaObject();
        QMetaMethod mm = senderMetaObject->method(idx);

        QJsonObject params;
        const QList<QByteArray> &paramNames = mm.parameterNames();

        QList<QByteArray>::const_iterator it = paramNames.constBegin();
        int i = 0;
        for (; it != paramNames.constEnd(); ++it)
        {
            QJsonValue value(convertValue(a[i + 1], mm.parameterType(i)));
            if (value.isNull())
            {
                qWarning("QJsonRpc: failed to convert signal argument %s(%s)",
                         qPrintable(mm.name()), qPrintable(*it));
                return;
            }
            else
            {
                params.insert(*it, value);
            }
            ++i;
        }

        /* FIXME: add serviceName */
        emit notify(QJsonRpcMessage::createNotification(mm.name(), params));

    } else {
        qWarning("QJsonRpc: cannot relay signals from parent %s(%p \"%s\") unless they are emitted in the object's thread %s(%p \"%s\"). "
                 "Current thread is %s(%p \"%s\").",
                 parent()->metaObject()->className(), parent(), qPrintable(parent()->objectName()),
                 parent()->thread()->metaObject()->className(), parent()->thread(), qPrintable(parent()->thread()->objectName()),
                 QThread::currentThread()->metaObject()->className(), QThread::currentThread(), qPrintable(QThread::currentThread()->objectName()));
    }
}

#include "qjsonrpcserviceprovider.extramoc"
#include "moc_qjsonrpcserviceprovider.cpp"

