#include <QMetaObject>
#include <QMetaClassInfo>
#include <QDebug>

#include "qjsonrpcserviceprovider.h"
#include "qjsonrpcsocket.h"
#include "qjsonrpcabstractserver_p.h"
#include "qjsonrpcabstractserver.h"


QJsonRpcAbstractServer::QJsonRpcAbstractServer(QJsonRpcAbstractServerPrivate &dd, QObject *parent)
    : QJsonRpcServiceProvider(dd, parent)
{
}

QJsonRpcAbstractServer::~QJsonRpcAbstractServer()
{
    Q_D(QJsonRpcAbstractServer);
     foreach (QJsonRpcSocket *client, d->clients)
        client->deleteLater();
    d->clients.clear();
}

#if QT_VERSION >= 0x050100 || QT_VERSION <= 0x050000
QJsonDocument::JsonFormat QJsonRpcAbstractServer::wireFormat() const
{
    Q_D(const QJsonRpcAbstractServer);
    return d->format;
}

void QJsonRpcAbstractServer::setWireFormat(QJsonDocument::JsonFormat format)
{
    Q_D(QJsonRpcAbstractServer);
    d->format = format;
}
#endif

void QJsonRpcAbstractServer::notify(const QString &method,
                                    const QJsonArray &params)
{
    QJsonRpcMessage notification =
        QJsonRpcMessage::createNotification(method, params);
    notify(notification);
}

void QJsonRpcAbstractServer::notify(const QJsonRpcMessage &message)
{
    Q_D(QJsonRpcAbstractServer);
    for (int i = 0; i < d->clients.size(); ++i)
        d->clients[i]->notify(message);
}

void QJsonRpcAbstractServerPrivate::_q_processMessage(const QJsonRpcMessage &message)
{
    Q_Q(QJsonRpcAbstractServer);
    QJsonRpcSocket *socket = static_cast<QJsonRpcSocket*>(q->sender());
    if (!socket) {
        qDebug() << Q_FUNC_INFO << "called without service socket";
        return;
    }

    QJsonRpcMessage reply = q->processMessage(message);
    if (reply.isValid())
        socket->notify(reply);
}

#include "moc_qjsonrpcabstractserver.cpp"
