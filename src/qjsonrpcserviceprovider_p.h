/*
 * Copyright (C) 2012-2013 Matt Broadstone
 * Copyright (C) 2013 Fargier Sylvain
 * Contact: http://bitbucket.org/devonit/qjsonrpc
 *
 * This file is part of the QJsonRpc Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
#ifndef QJSONRPCSERVICE_P_H
#define QJSONRPCSERVICE_P_H

#include <private/qobject_p.h>

#include <QHash>
#include <QPointer>
#include <QVarLengthArray>
#include <QStringList>

#include "qjsonrpcserviceprovider.h"
#include "qjsonrpcmessage.h"

class QJsonRpcSocket;
class QJsonRpcRelay;
class QJsonRpcServiceProvider;

struct QJsonRpcSlotCache
{
    struct ParameterInfo
    {
        ParameterInfo(const QString &name = QString(), int type = 0, bool out = false);

        int type;
        int jsType;
        QString name;
        bool out;
    };

    struct MethodInfo
    {
        MethodInfo();
        MethodInfo(const QMetaMethod &method);

        QVarLengthArray<ParameterInfo> parameters;
        int returnType;
        bool valid;
        bool hasOut;
        int index;
    };

    typedef QHash<QByteArray, QList<MethodInfo> > Hash;
    Hash hash;
};

class QJsonRpcServiceProviderPrivate : public QObjectPrivate
{
    Q_DECLARE_PUBLIC(QJsonRpcServiceProvider)
public:
    QJsonRpcServiceProviderPrivate();
    ~QJsonRpcServiceProviderPrivate();

    QByteArray serviceName(const QObject *service) const;
    QJsonRpcMessage deliverCall(QObject *object, const QJsonRpcMessage &request);
    QJsonRpcSlotCache *slotCache(const QObject *obj);

    void _q_objectDestroyed(QObject *object);

    static int qjsonRpcMessageType;
    static int convertVariantTypeToJSType(int type);

    QHash<QByteArray, QObject *> services;
    QHash<const QMetaObject *, QJsonRpcSlotCache *> cache;
    QJsonRpcRelay *relay;
};

#endif
